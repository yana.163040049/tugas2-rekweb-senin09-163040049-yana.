<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width,initial-scale=1" name="viewport">
		<title>Saung Awi</title>

        <link
            href="http://localhost/_saung_awi_v2/assets/plugin/bootstrap/css/bootstrap.css"
            rel="stylesheet">
        <link href="http://localhost/_saung_awi_v2/assets/css/style.css" rel="stylesheet">
    </head>
    <body>
    

    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container">
                <a class="back-button" href="http://localhost/_saung_awi_v2/home">
                    <div class="nav-button"><img src="http://localhost/_saung_awi_v2/assets/img/back-button.svg"></div>
                </a>
                <a class="next-button" href="#">
                    <div class="nav-button"><img src="http://localhost/_saung_awi_v2/assets/img/next-button.svg"></div>
                </a>         
        </div>
    </nav>


	
	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT -->

	<div class="container detile">
		<div class="content-galery-admin ">


			<div class="col-sm-12">
				<div class="thumbnail thumbnail-admin">

					<div class="content-button">
                        <a href="http://localhost/_saung_awi_v2/home/ubah/<?=  $makanan['id']; ?>"> <div class="button-admin"><img src="http://localhost/_saung_awi_v2/assets/img/edit.svg" ></a></div>
						<a href="http://localhost/_saung_awi_v2/home/hapus/<?=  $makanan['id']; ?>" onclick="return confirm('yakin?')";><div class="button-admin"><img src="http://localhost/_saung_awi_v2/assets/img/delete.svg" ></a></div>
                    </div>
					<img src="http://localhost/_saung_awi_v2/assets/img/<?=$makanan['gambar']; ?>" class="main-img">
					
					<div class="caption text-center">

						<h1><?=$makanan['nama']; ?></h1>
						<p>Deskripsi : <?=$makanan['deskripsi']; ?></p>
						<p>Jenis produk : <?=$makanan['jenis']; ?></p>
						<p>Stok :<?=$makanan['stok']; ?></p>
					</div>	

					<div class="text-desc">
						<h1>Rp. <?=$makanan['harga']; ?></h1>
					</div>

					<div class="view-detile text-center">
						<!-- <a href="<?php //echo $_GET["link"]; ?>" class="btn btn-default">view detile</a> -->
					</div>			
				</div>
			</div>
		</div>
	</div>

	<div class="footer"></div>
</body>
</html>