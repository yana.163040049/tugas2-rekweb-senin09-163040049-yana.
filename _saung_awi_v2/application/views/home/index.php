<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width,initial-scale=1" name="viewport">
        <title>Saung Awi</title>

        <link
            href="http://localhost/_saung_awi_v2/assets/plugin/bootstrap/css/bootstrap.css"
            rel="stylesheet">
        <link href="http://localhost/_saung_awi_v2/assets/css/style.css" rel="stylesheet">
    </head>
    <body>

        <nav class="navbar navbar-custom navbar-fixed-top">
            <div class="container">
                <a class="back-button" href="http://localhost/_saung_awi_v2/index.php">
                    <div class="nav-button"><img src="http://localhost/_saung_awi_v2/assets/img/back-button.svg"></div>
                </a>
                <a class="next-button" href="logout.php">
                    <div class="nav-button"><img src="http://localhost/_saung_awi_v2/assets/img/next-button.svg"></div>
                </a>

                <a class="next-button" href="http://localhost/_saung_awi_v2/home/tambah"">
	                <div class="nav-button"><img src="http://localhost/_saung_awi_v2/assets/img/insert.svg"></div>
	            </a>
                
            <div class="user-info" href="#">
                <p></p>

            </div>
        </div>
    </nav>

    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMALL HERO -->

    <div class="content text-center">
        <div class="col4">
            <img src="assets\img\logo.png" alt="" class="logo">


            <form action="" method="post">
                <input type="text"  name="keyword" class="form-control" placeholder="search" id="keyword" autofocus>
                <button class="btn btn-search" id="search" type="submit">submit</button>
            </form>
        </div>
    </div>


    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT-->
    <div id="wrap">
        <div class="container">
            <div class="content-galery-admin ">

                
                <!-- ********************************** PHP DATA TIDAK ADA -->

                <?php foreach($makanan as $mk) : ?>

                <div class="col-sm-3">
                    <a href="http://localhost/_saung_awi_v2/home/detail/<?=  $mk['id']; ?>">
                        <div class="thumbnail">
                            <img src="http://localhost/_saung_awi_v2/assets/img/<?=$mk['gambar'];?>" alt="">
                            <div class="caption">
                                <h4 class=""><?=$mk['nama'];?></h4>
                                <p>Stok : <?=$mk['stok'];?></p>
                                <h3>Rp. <?=$mk['harga'];?></h3>
                            </div>
                        </div>
                        <div class="content-button">


                                
								<a href="http://localhost/_saung_awi_v2/home/ubah/<?=  $mk['id']; ?>"> <div class="button-admin"><img src="http://localhost/_saung_awi_v2/assets/img/edit.svg" ></a></div>
								<a href="http://localhost/_saung_awi_v2/home/hapus/<?=  $mk['id']; ?>" onclick="return confirm('yakin?')";><div class="button-admin"><img src="http://localhost/_saung_awi_v2/assets/img/delete.svg" ></a></div>
							</div>
                    </a>
                </div>

                <?php endforeach; ?>

            </div>
            <!--container-galery-->
        </div>
        <!--container-->
    </div>

<!-- jika ajax menggunaka jquery -->
<script src="http://localhost/_saung_awi_v2/assets/plugin/jquery/jquery-3.1.1.min.js"></script>
</body>
</html>