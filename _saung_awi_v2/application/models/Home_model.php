<?php 

class Home_model extends CI_Model{
    public function getAllMakanan(){

        //cara pertama
        //$query = $this->db->get('makanan');
        //return $query->result_array();

        //cara kedua
        return $this->db->get('makanan')->result_array();

    
        
    }



    public function tambahDataMakanan(){


        $namaFile = $_FILES['gambar']['name'];
        $ukuranFile =  $_FILES['gambar']['size'];
        $errorFile =  $_FILES['gambar']['error'];
        $tmpFile =  $_FILES['gambar']['tmp_name'];
    

        // cek apakah gambar/bukan
        $gambar_aman = ['jpg','jpeg','png', 'gif'];
        $extensi_file = explode('.', $namaFile);
        $extensi_file = strtolower(end($extensi_file));

    
        //mengubah nama file gamba
        $nama_file_baru = uniqid(). '.' .$extensi_file;
    
        move_uploaded_file($tmpFile, './assets/img/' . $nama_file_baru);
        $image = $nama_file_baru;
    
    


        $data = [
            "nama" => $this->input->post('nama'),
            "deskripsi" => $this->input->post('deskripsi'),
            "jenis" => $this->input->post('jenis'),
            "harga" => $this->input->post('harga'),
            "stok" => $this->input->post('stok'),
            "gambar" => $nama_file_baru,
            ];

            var_dump($data);
        $this->db->insert('makanan' , $data);  
    }


    public function getMakananById($id){
        return $this->db->get_where('makanan', ['id' => $id])->row_array(); 
    }

    public function ubahDataMakanan(){
       


        $gambaLama = $makanan['gambarLama'];
        $namaFile = $_FILES['gambar']['name'];
        $ukuranFile =  $_FILES['gambar']['size'];
        $errorFile =  $_FILES['gambar']['error'];
        $tmpFile =  $_FILES['gambar']['tmp_name'];
    

        // cek apakah gambar/bukan
        $gambar_aman = ['jpg','jpeg','png', 'gif'];
        $extensi_file = explode('.', $namaFile);
        $extensi_file = strtolower(end($extensi_file));

    
        //mengubah nama file gamba
        $nama_file_baru = uniqid(). '.' .$extensi_file;
    
        move_uploaded_file($tmpFile, './assets/img/' . $nama_file_baru);
        $image = $nama_file_baru;

        


        $gambarLama = $this->input->post('gambarLama');
        

        if ($_FILES['gambar']['error'] == 4) {
            $nama_file_baru = $gambarLama;
        }
        

        $data = [
            "nama" => $this->input->post('nama', true),
            "deskripsi" => $this->input->post('deskripsi', true),
            "jenis" => $this->input->post('jenis', true),
            "harga" => $this->input->post('harga', true),
            "stok" => $this->input->post('stok', true),
            "gambar" => $nama_file_baru,
            ];


        $this->db->where('id', $this->input->post('id'));
        $this->db->update('makanan' , $data);
    }




    public function hapusDataMakanan($id){
        $this->db->where('id', $id);
        $this->db->delete('makanan');
    }


    public function carDataMakanan(){
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nama', $keyword);
        $this->db->or_like('jenis', $keyword);
        $this->db->or_like('harga', $keyword);
        $this->db->or_like('stok', $keyword);
        return $this->db->get('makanan')->result_array();

        
    }
    
}