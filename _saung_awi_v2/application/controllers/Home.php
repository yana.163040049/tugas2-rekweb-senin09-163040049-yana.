<?php
class Home extends CI_Controller
{
    //CONTRUK

    public function __construct(){
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $data['makanan'] = $this->Home_model->getAllMakanan();
        if ($this->input->post('keyword')) {
            $data['makanan'] = $this->Home_model->carDataMakanan();
        }


        
        $this->load->view("home/index.php", $data);
       
    }


    public function tambah(){


        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if ($this->form_validation->run() == FALSE) {

            $this->load->view("home/tambah.php");
        } else{
            $this->Home_model->tambahDataMakanan();
            $this->session->set_flashdata('flash', 'Ditambahkan');
            redirect('home');
        }  
    }


    public function ubah($id){

        $data['makanan'] = $this->Home_model->getMakananByid($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required');


        if ($this->form_validation->run() == FALSE) {
            $this->load->view("home/ubah.php", $data);
        } else{
            $this->Home_model->ubahDataMakanan();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('home');
        }

        
        
        

    }


    public function hapus($id){
        $this->Home_model->hapusDataMakanan($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('home');
    }



    public function detail($id){
        $data['makanan'] = $this->Home_model->getMakananByid($id);
         $this->load->view("home/detail.php", $data);
     }
        
        
}
